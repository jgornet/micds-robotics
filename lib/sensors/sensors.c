// Sensor functions and variables for robot.

// Color values of the NXT color sensor.
typedef enum {
	BLACK = 1,
	BLUE,
	GREEN,
	YELLOW,
	RED,
	WHITE,
} Color;

// Returns 1 if sensor perceives color inputted.
int isColor (Color color)
{
  if (SensorValue[colorSensor] == color)
    return 1;
  else
    return 0;
}
