#include "JoystickDriver.c"

task tankDrive()
{
	const int threshold = 18;

	while(true)
	{
		getJoystickSettings(joystick);
    if(abs(joystick.joy1_y2) > threshold)   // If the right analog stick's Y-axis readings are either above or below the threshold:
    {
      motor[rightMotor] = joystick.joy1_y2;         // Right Motor is assigned a power level equal to the right analog stick's Y-axis reading.
    }
    else                                    // Else if the readings are within the threshold:
    {
      motor[rightMotor] = 0;                        // Right Motor is stopped with a power level of 0.
    }


    if(abs(joystick.joy1_y1) > threshold)   // If the left analog stick's Y-axis readings are either above or below the threshold:
    {
      motor[leftMotor] = joystick.joy1_y1;         // Left Motor is assigned a power level equal to the left analog stick's Y-axis reading.
    }
    else                                    // Else if the readings are within the threshold:
    {
      motor[leftMotor] = 0;                        // Left Motor is stopped with a power level of 0.
    }
  }
}

task spinner()
{
	while(true){
		if(joy1Btn(6)){
			motor[spinnerMotor] = 50;
		}
		else if(joy1Btn(5)){
			motor[spinnerMotor] = -50;
		}
		else {
			motor[spinnerMotor] = 0;
		}
  }
}
