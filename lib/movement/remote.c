#include <JoystickDriver.c>

int power; int angle;

// Defines RC remote driving controls. Contains thresholds and driving angles.
task drivingControl()
{
	while(true)
	{
		const float CHAR_TO_MOTOR = -.7812;
		const int threshold = 10;
		getJoystickSettings(joystick);
		power = joystick.joy1_y1 * CHAR_TO_MOTOR;
		angle = joystick.joy1_x2;

    if(power < (-1) * threshold){  //Backward movement
        if (angle < (-1)*threshold){
                motor[leftMotor] = power + abs(power * angle / 129);
                motor[rightMotor] = power; // Turning left
      	}	else if (angle > threshold){
                motor[leftMotor] = power; // Turning right
                motor[rightMotor] = power + abs(power * angle / 129);
        } else {
                motor[rightMotor] = power; // Moving backward
                motor[leftMotor] = power;
        }
    } else if (power > threshold){ // Moving Forward
        if (angle > threshold){
                motor[rightMotor] = power - abs(power * angle / 129);
                motor[leftMotor] = power; // Turning right
        } else if (angle < (-1)*threshold){
                motor[rightMotor] = power; // Turning left
                motor[leftMotor] = power - abs(power * angle / 129);
        } else {
                motor[rightMotor] = power; //Moving forward
                motor[leftMotor] = power;
        }
    } else {
        motor[rightMotor] = 0;
        motor[leftMotor] = 0;
    }
    // Power Variable Debugging
    if (power > 100 || power < -100){
    	writeDebugStreamLine("Power Variable Failure.\n Value: %d", power);
    	PlaySound(soundBlip);
    }
	}
task tankControl()
{

}
}
