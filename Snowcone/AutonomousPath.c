#pragma config(Hubs,  S1, HTMotor,  HTMotor,  HTMotor,  none)
#pragma config(Hubs,  S4, HTServo,  none,     none,     none)
#pragma config(Sensor, S2,     IRSensor_L,     sensorI2CCustom)
#pragma config(Sensor, S3,     IRSensor_R,     sensorI2CCustom)
#pragma config(Motor,  motorA,          Led1,          tmotorNXT, PIDControl, encoder)
#pragma config(Motor,  motorB,          Led2,          tmotorNXT, PIDControl, encoder)
#pragma config(Motor,  mtr_S1_C1_1,     leftMotor,     tmotorTetrix, openLoop)
#pragma config(Motor,  mtr_S1_C1_2,     rightMotor,    tmotorTetrix, openLoop, reversed)
#pragma config(Motor,  mtr_S1_C2_1,     liftMotor1,    tmotorTetrix, openLoop)
#pragma config(Motor,  mtr_S1_C2_2,     liftMotor2,    tmotorTetrix, openLoop)
#pragma config(Motor,  mtr_S1_C3_1,     spinnerMotor,  tmotorTetrix, openLoop)
#pragma config(Motor,  mtr_S1_C3_2,     bucketMotor,   tmotorTetrix, openLoop)
#pragma config(Servo,  srvo_S4_C1_1,    tubeServo,            tServoStandard)
#pragma config(Servo,  srvo_S4_C1_2,    bucketServo,          tServoStandard)
#pragma config(Servo,  srvo_S4_C1_3,    servo3,               tServoNone)
#pragma config(Servo,  srvo_S4_C1_4,    servo4,               tServoNone)
#pragma config(Servo,  srvo_S4_C1_5,    servo5,               tServoNone)
#pragma config(Servo,  srvo_S4_C1_6,    servo6,               tServoNone)
//*!!Code automatically generated by 'ROBOTC' configuration wizard               !!*//

#include "JoystickDriver.c"
#define LAG_TIME  200

task main()
{
	TFileHandle 	hFileHandle;
	TFileIOResult nIOResult;
	string				sFileName = "path.txt";
	string 				str = "";
	int 					nFileSize = 60000;
	const int			threshold = 10;
	const float 	power = .79365;

	Delete(sFileName, nIOResult);
	OpenWrite(hFileHandle, nIOResult, sFileName, nFileSize);

	while(true) {
		getJoystickSettings(joystick);
		if(abs(joystick.joy1_y2) > threshold) {
      motor[rightMotor] = joystick.joy1_y2*power;
    	WriteText(hFileHandle, nIOResult, "motor[rightMotor] = ");
    	StringFormat(str, "%d", joystick.joy1_y2*power);
    	WriteText(hFileHandle, nIOResult, str);
    	WriteText(hFileHandle, nIOResult, ";\n");
    } else {
      motor[rightMotor] = 0;
    }

    if(abs(joystick.joy1_y1) > threshold) {
      motor[leftMotor] = joystick.joy1_y1*power;
    	WriteText(hFileHandle, nIOResult, "motor[leftMotor] = ");
    	StringFormat(str, "%d", joystick.joy1_y1*power);
    	WriteText(hFileHandle, nIOResult, str);
    	WriteText(hFileHandle, nIOResult, ";\n");
    } else {
      motor[leftMotor] = 0;
    }

    if(abs(joystick.joy2_y1) > threshold) {
      motor[liftMotor1] = joystick.joy2_y1*power;
      motor[liftMotor2] = joystick.joy2_y1*power;
    	WriteText(hFileHandle, nIOResult, "motor[liftMotor1] = ");
    	StringFormat(str, "%d", joystick.joy2_y1*power);
    	WriteText(hFileHandle, nIOResult, str);
    	WriteText(hFileHandle, nIOResult, ";\n");
    	WriteText(hFileHandle, nIOResult, "motor[liftMotor2] = ");
    	StringFormat(str, "%d", joystick.joy2_y1*power);
    	WriteText(hFileHandle, nIOResult, str);
    	WriteText(hFileHandle, nIOResult, ";\n");
    } else {
      motor[liftMotor1] = 0;
      motor[liftMotor2] = 0;
    }

    if(abs(joystick.joy2_y2) > threshold) {
      motor[bucketMotor] = joystick.joy2_y2;
    	WriteText(hFileHandle, nIOResult, "motor[bucketMotor] = ");
    	StringFormat(str, "%d", joystick.joy2_y2);
    	WriteText(hFileHandle, nIOResult, str);
    	WriteText(hFileHandle, nIOResult, ";\n");
    } else {
      motor[bucketMotor] = 0;
    }

    if(joy2Btn(6)) {
      servo[bucketServo] = 180;
      WriteText(hFileHandle, nIOResult, "servo[bucketServo] = 180;\n");
    }

    if(joy2Btn(7)) {
      servo[bucketServo] = 0;
      WriteText(hFileHandle, nIOResult, "servo[bucketServo] = 0;\n");
    }

    wait1Msec(LAG_TIME);
		if((abs(joystick.joy1_y1) < threshold) && (abs(joystick.joy1_y2) < threshold) \
		  && (abs(joystick.joy2_y1) < threshold) && (abs(joystick.joy2_y2) < threshold))
		  continue;
    WriteText(hFileHandle, nIOResult, "wait1Msec(LAG_TIME);\n");
  }
}
