#pragma config(Hubs,  S1, HTMotor,  HTMotor,  HTMotor,  HTServo)
#pragma config(Sensor, S3,     colorSensor,    sensorCOLORFULL)
#pragma config(Sensor, S4,     sonarSensor,    sensorSONAR)
#pragma config(Motor,  motorA,          clamp,         tmotorNXT, PIDControl, encoder)
#pragma config(Motor,  mtr_S1_C1_1,     rightMotor,    tmotorTetrix, openLoop, reversed)
#pragma config(Motor,  mtr_S1_C1_2,     leftMotor,     tmotorTetrix, openLoop)
#pragma config(Motor,  mtr_S1_C2_1,     spinnerMotor,  tmotorTetrix, openLoop)
#pragma config(Motor,  mtr_S1_C2_2,     liftMotor,     tmotorTetrix, openLoop)
#pragma config(Motor,  mtr_S1_C3_1,     bucketMotor,   tmotorTetrix, openLoop)
#pragma config(Motor,  mtr_S1_C3_2,     motorI,        tmotorTetrix, openLoop)
#pragma config(Servo,  srvo_S1_C4_1,    bucketOpen,           tServoStandard)
#pragma config(Servo,  srvo_S1_C4_2,    tubeLock,             tServoStandard)
#pragma config(Servo,  srvo_S1_C4_3,    servo3,               tServoNone)
#pragma config(Servo,  srvo_S1_C4_4,    servo4,               tServoNone)
#pragma config(Servo,  srvo_S1_C4_5,    servo5,               tServoNone)
#pragma config(Servo,  srvo_S1_C4_6,    servo6,               tServoNone)
//*!!Code automatically generated by 'ROBOTC' configuration wizard               !!*//

const char rampValue = 1;

static void approachKickstand (char isRamp);
static void circleKickstand (char isRamp);

task main()
{
	approachKickstand(rampValue);
	circleKickstand(rampValue);
}

static void approachKickstand (char isRamp)
{
	int a, b;

	if (isRamp) {
		a = SensorValue[colorSensor];
		wait1Msec(250);
		b = SensorValue[colorSensor];
		wait1Msec(250);

		while(true){
			motor[leftMotor] = 10;
			motor[rightMotor] = 10;
			wait1Msec(50);
			a = SensorValue[colorSensor];
			if (a != b)
				break;
			motor[leftMotor] = 10;
			motor[rightMotor] = 10;
			wait1Msec(50);
			b = SensorValue[colorSensor];
			if (a != b)
				break;
		}
	} else {
		int distance;
		while (distance > 20) {
			motor[leftMotor] = 10;
			motor[rightMotor] = 10;
			distance = SensorValue[sonarSensor];
			wait1Msec(50);
		}
	}
}

static void circleKickstand (char isRamp)
{
	if (isRamp) {
		motor[rightMotor] = 20;
		motor[leftMotor] = 10;
		wait1Msec(8000);
	} else {
		motor[rightMotor] = 10;
		motor[leftMotor] = 20;
		wait1Msec(8000);
	}
}
